# 自主查勘 H5 App

采用 Ionic vue 框架

Ionic官网 https://ionicframework.com/

```bash
# 全局安装Ionic
npm install -g @ionic/cli@latest native-run cordova-res
# 安装项目依赖
npm install
# 启动浏览器开发
ionic serve
```

安装打包环境准备 https://ionicframework.com/docs/developing/android

```bash
# 添加android支持
# ionic capacitor add android
# 构建前端代码
# ionic build
# ionic capacitor build android
# 前端代码发布到android项目
ionic capacitor copy android
# Live reload测试
ionic capacitor run android -l --host=YOUR_IP_ADDRESS
ionic capacitor run android -l --external
```

## Icon and Splash screen

```bash
# 准备文件
/resources/
├── /android/
├── ── icon-background.png
├── ── icon-foreground.png
├── icon.png
└── splash.png

npm install -g cordova-res
# cordova-res ios --skip-config --copy
cordova-res android --skip-config --copy
```
