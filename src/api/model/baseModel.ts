export interface BasicPageResult<T extends any>  {
  pages: number
  pageSize: number
  total:number
  size:number
  searchCount:boolean
  records:T[]
}

export interface BasicFetchResult<T extends any> {
  items: T[]
  total: number
}

export interface BasicPageParams {
  pageNo: number
  pageSize: number
}


export interface BasicEnumItem {
  value: number
  label: string
}
