import request from '@/utils/request'
import {
  JobPageListResultModel,
  JobParams,
  JobPageListParams, JobModel, JobStatusEnumModel,createJobParams,GetUserInfoModel
} from '@/api/file/model/jobModel'
import {FileLinkOrderParams, FileModel, FilePageListParams} from "@/api/file/model/fileModel";
import {Page} from "@/types/page";

enum Api {
  Job = '/v1/app/work-order/list',
  GetUserInfo = '/v1/profile',
}

export function getJobPageList(params?: JobPageListParams) {
  return request.get<JobPageListResultModel>(Api.Job)
}

export const createJob = (params: createJobParams): Promise<void> => {
  return request.post(`v1/survey/workorder`,params)
}

// export function getUserInfo() {
//   return request.get<GetUserInfoModel>( Api.GetUserInfo )
// }

export const getUserInfo = (): Promise<GetUserInfoModel> => {
  return request.get('/v1/profile')
}

