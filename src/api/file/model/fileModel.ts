import {BasicEnumItem, BasicPageParams, BasicPageResult} from '../../model/baseModel'

export interface FileModel {
  id: number
  fileTypeValue: number
  fileTypeLabel: string
  fileTypeTag: string
  fileSource: number
  fileSourceLabel: string
  streamId: string
  fileId: string
  mediaUrl: string
  coverUrl: string
  app: string
  appId: string
  appName: string
  fileName: string
  fileFormatValue: string
  fileFormatLabel: string
  fileSizeByte: number
  fileSizeFormat: string
  recordStartTime: string
  recordEndTime: string
  recordDuration: number
  isNeedDownload: number
  isDownloadSuccess: number
  taskId: string
  creator: string
  createTime: string
  mediaDownloadUrl: string
  cosOutputKeyUrl: string
  cosFileUrl: string
  cosJobStatusValue: number
  cosJobStatusLabel: string
}

export interface FileParams {
  workOrderId: string
}

export interface FileLinkOrder {
  workOrderId: string,
  fileIds: any[] | undefined
}

export type FileLinkOrderParams = FileLinkOrder;

export type FilePageListResultModel = BasicPageResult<FileModel>;

export type FileListResultModel = FileModel[];

export type FileTypeListResultModel = BasicEnumItem[];

export type FilePageListParams = BasicPageParams & FileParams;
