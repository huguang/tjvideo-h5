import {BasicEnumItem, BasicPageParams, BasicPageResult} from '../../model/baseModel'
import {FileLinkOrder} from "@/api/file/model/fileModel";

export interface JobModel {
    id: number
    name: string
    number: string
    content: string
    startTime: string
    endTime: string
    sourceValue: string
    sourcesLabel: string
    statusValue: string
    statusLabel: string
    createTime: string
    updateTime: string
    creator: string
    updater: string
    licensePlate: string
    mediaFileCount: number
}

export interface JobParams {
    creatorName: string
    handleName: string
    startTime: string
    endTime: string
    status: string
    name: string
}

export interface createCaseParams {
    number: string
    licensePlate: string
    content: string
    handleUserId: number
}

export interface GetUserInfoModel {
    // 用户id
    id: string | number
    // 用户名
    username: string
    realName: string
    // 真实名字
    name: string;
    email?: string;
    mobile?: string;
    statusLabel: string;
    statusValue: string;
    updateTime: string;

}

export type JobPageListResultModel = BasicPageResult<JobModel>;

export type JobListResultModel = JobModel[];

export type JobStatusEnumModel = BasicEnumItem[];

export type JobPageListParams = BasicPageParams & JobParams;

export type createJobParams = createCaseParams;

export type AccountListGetResultModel = BasicPageResult<GetUserInfoModel>;


