import request from '@/utils/request'
import { Page } from '@/types/page'
import {FileLinkOrderParams, FileModel} from '@/api/file/model/fileModel'
import {
    FilePageListParams,
} from './model/fileModel'

export const getFilePageList = (params: FilePageListParams): Promise<Page<FileModel>> => {
    return request.get('/v1/survey/media-file/work-order/list', {params})
}

export const fileLinkOrder = (params: FileLinkOrderParams): Promise<void> => {
    return request.post(`v1/survey/media-file/link-to-work-order`,params)
}


