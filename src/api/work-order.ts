import request from '../utils/request'
import { Page } from '@/types/page'
import { WorkOrder } from '@/types/work-order'

export const workStart = (id: number): Promise<void> => {
  return request.post(`/v1/app/${id}/start`)
}

export const workFinish = (id: number): Promise<void> => {
  return request.post(`/v1/app/${id}/finish`)
}

export const getOrders = (pageNo = 1, pageSize = 10): Promise<Page<WorkOrder>> => {
  return request.get('/v1/app/work-order/list', { params: { pageNo, pageSize }})
}

export const getOrder = (id: number): Promise<WorkOrder> => {
  return request.get(`/v1/app/work-order/${id}`)
}

export const getOrderByNumber = (number: string): Promise<WorkOrder> => {
  return request.get(`/v1/survey/workorder-number/${number}`)
}

export const takePhoto = (id: number): Promise<void> => {
  return request.post(`/v1/app/${id}/controller/take-photo`)
}

export const recordVideoStart = (id: number): Promise<void> => {
  return request.post(`/v1/app/${id}/controller/record-video/start`)
}

export const recordVideoStop = (id: number): Promise<void> => {
  return request.post(`/v1/app/${id}/controller/record-video/stop`)
}
