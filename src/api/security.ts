import request from '../utils/request'

export interface LoginForm {
  username: string;
  password: string;
}

export interface LoginResult {
  code: number;
  success: boolean;
  message: string;
  token: string;
}

export const login = (params: LoginForm): Promise<LoginResult> => {
  return request.post('/v1/app/login', params)
}
