import axios from 'axios'
import router from '@/router'

const request = axios.create({
  baseURL: 'https://tjvideo.dev.tjvideo.cn:9012/api/user',
  timeout: 5000,
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  withCredentials: false
})

request.interceptors.request.use((config) => {
  const accessToken = localStorage.getItem('access-token')
  if (config && config.headers && accessToken) {
    config.headers['Authorization'] = 'Bearer ' + accessToken
  }
  return config
}, (error) => {
  return Promise.reject(error)
})

request.interceptors.response.use((resp) => {
  return Promise.resolve(resp.data)
}, (error) => {
  if (error.response && error.response.status === 401) {
    // 刷新token失败，可以直接跳转登录页
    localStorage.removeItem('access-token')
    router.replace('/login')
    return Promise.reject(error.response.data)
  }

  if (error.response.data.constructor === ({}).constructor) {
    return Promise.reject(error.response.data.message)
  } else {
    return Promise.reject('服务端错误')
  }
})

export default request
