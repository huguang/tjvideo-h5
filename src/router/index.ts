import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import Login from '../views/Login.vue'
import Tabs from '../views/Tabs.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/tabs'
  },
  {
    path: '/login',
    component: Login,
  },
  {
    path: '/tabs',
    component: Tabs,
    children: [
      {
        path: '',
        redirect: '/tabs/tab-case'
      },
      {
        path: '/tabs/tab-case',
        name: 'tabCase',
        component: () => import('@/views/TabCase.vue')
      },
      {
        path: '/tabs/tab-file',
        component: () => import('@/views/TabFile.vue')
      },
      {
        path: '/tabs/tab-video',
        component: () => import('@/views/TabVideo.vue')
      },
      {
        path: '/tabs/tab-setting',
        component: () => import('@/views/TabSetting.vue')
      },
      {
        path: '/tabs/fileList/:id',
        component: () => import('@/views/FileDetail.vue'),
        meta:{
          hideMenu: true
        }
      }
    ]
  },
  {
    path: '/case',
    component: () => import('@/views/Case.vue')
  },
  {
    path: '/case-capture',
    component: () => import('@/views/CaseCapture.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
