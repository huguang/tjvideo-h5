import { User } from '@/types/user'

export interface WorkOrder {
  id: number;
  name: string;
  number: string;
  content: string;
  startTime: string;
  endTime: string;
  sourceValue: number;
  sourcesLabel: string;
  statusValue: number;
  statusLabel: string;
  createTime: string;
  updateTime: string;
  creator: string;
  updater: string;
  isRead: number;
  isReadLabel: string;
  licensePlate: string;
  mediaFileCount: number;
  mediaFiles: [],
  startRecordTime: string;
  creatorUser: Array<User>;
  handleUser: Array<User>;
}
