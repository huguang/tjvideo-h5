export interface Page<T> {
  current: number; // 当前页
  size: number; // 每页数量,
  pages: number; // 总页数
  total: number; // 总记录数
  records: Array<T>; // 记录
}
