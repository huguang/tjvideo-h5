export interface User {
  id: number;
  username: string;
  mobile: string;
  email: string;
  name: string;
  statusValue: number;
  statusLabel: string;
  createTime: string;
  updateTime: string;
  creator: string;
  updater: string;
  roles: [],
  departments: [],
  permissions: []
}
