export interface BasicPageResult<T extends any>  {
    pages: number
    pageSize: number
    total:number
    size:number
    searchCount:boolean
    records:T[]
}

export interface MeetingInfoModel {
    createTime: string
    id: number
}

export type MeetingPageListResultModel = BasicPageResult<MeetingInfoModel>;