import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: '查勘管理',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
